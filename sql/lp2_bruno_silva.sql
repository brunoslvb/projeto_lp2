CREATE DATABASE lp2_bruno_silva;

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `email` varchar(150) NOT NULL,
  `telefone` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `email`, `telefone`, `last_modified`) VALUES
(3, 'Bruno da Silva', 'brunosilva2365@gmail.comaa', 1111111, '2019-10-07 01:02:36'),
(8, 'Bruno da Silva Barros', 'brunosilva2365@gmail.com', 124578963, '2019-10-07 01:07:24'),
(9, 'TESTE', 'brunosilva540@yahoo.com.br', 1111, '2019-10-07 01:33:52'),
(10, 'TESTE2', 'brunosilva540@yahoo.com.br', 2222, '2019-10-07 01:34:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `subtitulo` varchar(100) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `titulo`, `subtitulo`, `last_modified`) VALUES
(1, 'Naah Fashion Modas', 'Venha experimentar as melhores roupas do Brasil', '2019-10-07 01:02:52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `descricao` varchar(512) NOT NULL,
  `preco` decimal(18,2) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `nome`, `descricao`, `preco`, `last_modified`) VALUES
(1, 'TESTE', 'Texto é um conjunto de palavras e frases encadeadas que permitem interpretação e transmitem uma mensTexto é um conjunto de palavras e frases encadeadas que permitem interpretação e transmitem uma mensagem. É qualquer obra escrita em versão original e que constitui um livro ou um documento escrito. Um texto é uma unidade linguística de extensão superior à frase.', '12.30', '2019-10-07 00:15:20'),


--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`);

