<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutoModel extends CI_Model{

    function __construct(){
        $this->load->library('util/Validator');
    }

    public function getColumns(){
        $columns = array('id', 'nome', 'descricao', 'preco');
        return $columns;
    }
}
