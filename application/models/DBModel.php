<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DBModel extends CI_Model{

    function __construct(){
        $this->load->library('Crud');
        $this->load->model('ClienteModel', 'cliente');
        $this->load->model('LojaModel', 'loja');
        $this->load->model('HomeModel', 'home');
        $this->load->model('ProdutoModel', 'produto');
    }

    public function lista($table, $cond){
        $v = $this->crud->get($table);

        $html = '<br><table class="table border table-hover">';
        $html .= $this->table_header($table);
        $html .= $this->table_body($v, $table, $cond);
        $html .= '</table>';
        return $html;
    }

    private function table_body($v, $table, $cond){

        $columns = $this->$table->getColumns();

        $html = '<tr>';
        foreach ($v as $cliente) {
            for ($i=0; $i < count($columns); $i++) {
                $html .= '<td>'.$cliente[$columns[$i]].'</td>';
            }
            $html .= '<td width="120px">'.$this->action_buttons($cliente[$columns[0]], $table, $cond).'</td>';
            $html .= '</tr>';
        }

        return $html;
    }

    private function action_buttons($id, $table, $cond){
        $html = '<a href="'.base_url($table.'/edit/'.$id).'">';
        $html .= '<i class="far fa-edit indigo-text mr-3"></i></a>';

        if ($cond == true) {
            $html .= '<a href="'.base_url($table.'/delete/'.$id).'">';
            $html .= '<i class="far fa-trash-alt red-text mr-3"></i></a>';
            $html .= '<a href="'.base_url($table.'/formCadastro').'">';
            $html .= '<i class="fas fa-user-plus"> </i></a>';
        } else {
            $html .= '';
        }
        return $html;
    }

    private function table_header($table){
        $columns = $this->$table->getColumns();

        $html  = '<tr>';
        $html .= '<td>#</td>';

        for ($i=1; $i < count($columns); $i++) {
            $html .= '<td>'.ucfirst($columns[$i]).'</td>';
        }

        $html .= '<td>Ações</td>';
        $html .= '</tr>';
        return $html;
    }

    public function salvar($table){
        if(sizeof($_POST) == 0) return;

        if ($table == 'cliente') {
          if($this->validator->cliente_validate()){
              $columns = $this->$table->getColumns();

              for ($i=1; $i < count($columns); $i++) {
                  $data[$columns[$i]] = $this->input->post($columns[$i]);
              }
              $this->crud->set($table, $data);
              redirect('/');
          }
        } elseif ($table == 'home') {
          if($this->validator->home_validate()){
              $columns = $this->$table->getColumns();

              for ($i=1; $i < count($columns); $i++) {
                  $data[$columns[$i]] = $this->input->post($columns[$i]);
              }
              $this->crud->set($table, $data);
              redirect('administration/');
          }
        } elseif ($table == 'produto') {
            if($this->validator->produto_validate()){
              $columns = $this->$table->getColumns();

              for ($i=1; $i < count($columns); $i++) {
                  $data[$columns[$i]] = $this->input->post($columns[$i]);
              }
              $this->crud->set($table, $data);
              redirect('administration/');
            }
        }
    }

    public function carregar($table, $id){
        $_POST = $this->crud->get_by_id($table, $id);
    }

    public function atualizar($table, $id){
        if(sizeof($_POST) == 0) return;

        //if($this->getValidate($table)){

        if ($table == 'cliente') {
          if($this->validator->cliente_validate()){
              $columns = $this->$table->getColumns();

              for ($i=1; $i < count($columns); $i++) {
                  $data[$columns[$i]] = $this->input->post($columns[$i]);
              }

              $status = $this->crud->update($table, $data, $id);
              if($status) redirect('administration/');
          }
        } elseif ($table == 'home') {
          if($this->validator->home_validate()){
              $columns = $this->$table->getColumns();

              for ($i=1; $i < count($columns); $i++) {
                  $data[$columns[$i]] = $this->input->post($columns[$i]);
              }

              $status = $this->crud->update($table, $data, $id);
              if($status) redirect('administration/');
          }
        } elseif ($table == 'produto') {
            if($this->validator->produto_validate()){
              $columns = $this->$table->getColumns();

              for ($i=1; $i < count($columns); $i++) {
                  $data[$columns[$i]] = $this->input->post($columns[$i]);
              }

              $status = $this->crud->update($table, $data, $id);
              if($status) redirect('administration/');
            }
        }
    }

    public function delete($table, $id){
        $this->crud->delete($table, $id);
    }

    public function getValidate($table){
        if ($table == 'cliente') {
            $this->validator->cliente_validate();
        } elseif ($table == 'home') {
            $this->validator->home_validate();
        }
    }

    public function Jumbo($table){

        $v = $this->crud->get($table);
        $columns = $this->$table->getColumns();

        $html = '';
        $aux = 1;

        foreach ($v as $cliente) {
            $html .= '<div class="jumbotron text-center hoverable p-2">';
            for ($i=1; $i < count($columns); $i++) {
                $html .='
                    <div class="row">
                      <div class="col-md-4 offset-md-1 mx-2 my-2">
                        <div class="view overlay">
                          <img src="https://mdbootstrap.com/img/Photos/Slides/img%20('.$aux.').jpg" class="img-fluid" alt="Sample image for first version of blog listing">
                          <a>
                            <div class="mask rgba-white-slight"></div>
                          </a>
                        </div>
                      </div>
                      <div class="col-md-7 text-md-left ml-3 mt-3">
                        <a href="#!" class="green-text">
                            <!--<h6 class="h6 pb-1"><i class="fas fa-desktop pr-1"></i> Work</h6>-->
                        </a>
                        <h4 class="h4 mb-4">'.$cliente[$columns[$i]].'</h4>
                        <p class="font-weight-normal">'.$cliente[$columns[$i+1]].'</p>
                        <a class="btn btn-success">'.$cliente[$columns[$i+2]].'</a>
                      </div>
                    </div>';
                    $i = $i+2;
                    $aux += $aux;
            }
            $html .= '</div>';
        }
        return $html;
    }




}
