<?php

class Card{

  public function Jumbo($table){

      $this->load->library('Crud');
      $this->load->model('ProdutoModel', 'produto');

      $v = $this->crud->get($table);
      $columns = $this->$table->getColumns();

      $html = '';

      foreach ($v as $cliente) {
          $html .= '<div class="jumbotron text-center hoverable p-4">';
          for ($i=1; $i < count($columns); $i++) {
              $html .='
                  <div class="row">
                    <div class="col-md-4 offset-md-1 mx-3 my-3">
                      <div class="view overlay">
                        <img src="https://mdbootstrap.com/img/Photos/Slides/img%20('.$i.').jpg" class="img-fluid" alt="Sample image for first version of blog listing">
                        <a>
                          <div class="mask rgba-white-slight"></div>
                        </a>
                      </div>
                    </div>
                    <div class="col-md-7 text-md-left ml-3 mt-3">
                      <a href="#!" class="green-text">
                          <!--<h6 class="h6 pb-1"><i class="fas fa-desktop pr-1"></i> Work</h6>-->
                      </a>
                      <h4 class="h4 mb-4">'.$cliente[$columns[$i]].'</h4>
                      <p class="font-weight-normal">'.$cliente[$columns[$i+1]].'</p>
                      <a class="btn btn-success">'.$cliente[$columns[$i+2]].'</a>
                    </div>
                  </div>';
                  $i = $i+2;
          }
          $html .= '</div>';
      }
      return $html;
  }
}
