  <!-- Full Page Intro -->
  <div class="view jarallax background-home" style="background-image: url('assets/mdb/img/cabides.jpg')" data-jarallax='{"speed": 0.5}'>
    <!-- Mask & flexbox options-->
    <div class="mask rgba-black-strong d-flex justify-content-center align-items-center">
      <!-- Content -->
      <div class="container">
        <!--Grid row-->
        <div class="row wow fadeIn">
          <!--Grid column-->
          <div class="col-md-12 text-center">
            <h1 class="display-4 font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInUp white-text"><?= $titulo ?></h1>
            <!--<h1 class="display-6 font-weight-bold mb-0 wow fadeInUp white-text">Our New Course is Ready</h1>-->
            <h5 class="pt-md-5 pt-sm-2 pb-md-5 pb-sm-3 pb-5 wow fadeInUp white-text"><?= $subtitulo ?></h5>
            <div class="wow fadeInUp" data-wow-delay="0.4s">
              <a class="btn btn-white btn-rounded black-text" href="<?= site_url('cliente/formCadastro');?>"><i class="fas fa-user left"></i>Cadastre-se</a>
              <a class="btn btn-outline-white" href="<?= site_url('produto/catalogo');?>"><i class="fas fa-list left"></i> Catálogo</a>
            </div>
          </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </div>
      <!-- Content -->
    </div>
    <!-- Mask & flexbox options-->
  </div>
  <!-- Full Page Intro -->
