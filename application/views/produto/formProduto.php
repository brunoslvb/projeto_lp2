
<div class="container col-md-5 mt-4">
  <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <form class="text-center border border-light p-5" method="POST">
      <p class="h4 mb-4">Cadastre-se</p>
      <div class="form-row mb-2">
          <div class="col">
              <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome" value="<?= set_value('nome') ?>">
          </div>
          <div class="col">
            <input type="text" id="preco" name="preco" class="form-control mb-4" placeholder="Preço do Produto" value="<?= set_value('preco') ?>">
          </div>
      </div>
      <input type="text" id="descricao" name="descricao" class="form-control" placeholder="Descrição" value="<?=set_value('descricao')?>">

      <!--<div class="input-group mt-3">
        <div class="custom-file">
          <input type="file" class="custom-file-input" id="imagem" id="imagem" aria-describedby="inputGroupFileAddon01">
          <label class="custom-file-label" for="imagem"></label>
        </div>
      </div>-->
      <button class="btn btn-info my-4 btn-block" type="submit">Cadastrar</button>
  </form>
</div>
