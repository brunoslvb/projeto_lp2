
<div class="container col-md-5 mt-4">
    <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <form class="text-center border border-light p-5" method="POST">
        <p class="h4 mb-4">Cadastre-se</p>
        <div class="form-row mb-4">
            <div class="col">
                <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome Completo" value="<?= set_value('nome') ?>">
            </div>
            <div class="col">
                <input type="number" id="telefone" name="telefone" class="form-control" placeholder="Telefone" value="<?= set_value('telefone') ?>">
            </div>
        </div>
        <input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail" value="<?= set_value('email') ?>">
        <button class="btn btn-info my-4 btn-block" type="submit">Cadastrar</button>
    </form>
</div>
