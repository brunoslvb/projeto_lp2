<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Funcionario extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('FuncionarioModel', 'funcionario');
    }

    public function index(){
        // lista de funcionarios
    }

    public function cadastro(){
        $this->funcionario->salva();
        $data['forms'] = $this->load->view('funcionario/form_dados_pessoais', null, true);
        $data['forms'] .= $this->load->view('funcionario/form_endereco', null, true);
        $html = $this->load->view('funcionario/form_base', $data, true);
        $this->show($html);
    }

    public function edita($id){

    }

    public function delete($id){

    }

}