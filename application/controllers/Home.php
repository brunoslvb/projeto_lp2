<?php

// um controlador é um gerador de páginas.
class Home extends MY_Controller{

		function __construct(){
        parent::__construct();
        $this->load->model('HomeModel', 'home');
        $this->load->model('DBModel');
        $this->load->library('Crud');
    }

		public function index(){
				$this->load->view('common/header');
				$this->load->view('home/navbar_home');
				$data = $this->crud->get_by_id('home', 1);
				$this->load->view('home/container_home', $data);
				$this->load->view('common/footer');
		}

		public function formHome(){
				 $this->DBModel->salvar('home');
				 $html = $this->load->view('home/form', null, true);

				 $this->show($html);
		}

		public function edit($id){
        $this->DBModel->atualizar('home', $id);
        $this->DBModel->carregar('home', $id);

        $html = $this->load->view('home/form', null, true);
        $this->show($html);
    }

    public  function delete($id){
        $this->DBModel->delete('home', $id);
        redirect('administration/');
    }
}
