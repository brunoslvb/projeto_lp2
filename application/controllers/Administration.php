<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends MY_Controller{

  function __construct(){
      parent::__construct();
      $this->load->library('Crud');
      $this->load->model('DBModel');
      $this->load->library('component/Panel', 'panel');
  }

    public function index(){

    		$this->load->view('common/header');
    		$this->load->view('common/navbar');
        $data['produtos'] = $this->DBModel->lista('produto', true);
        $data['clientes'] = $this->DBModel->lista('cliente', true);
        $tabelas['tabelas'] = $this->panel->tablePanel($data);
        $tabelas['home'] = $this->DBModel->lista('home', false);

        $this->load->view('adm/admin', $tabelas);
    		$this->load->view('common/footer');
    }

}
