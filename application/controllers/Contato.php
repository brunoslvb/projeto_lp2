<?php

// um controlador é um gerador de páginas.
class Contato extends MY_Controller{

		function __construct(){
        parent::__construct();
        $this->load->model('HomeModel', 'home');
        $this->load->model('DBModel');
        $this->load->library('Crud');
    }

    public function index(){
				$this->load->view('common/header');
				$this->load->view('home/navbar_home');
				$this->load->view('contato/teste');
				$this->load->view('common/footer');
		}
}
